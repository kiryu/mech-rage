﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Kinect = Windows.Kinect;

public class BodySourceView : MonoBehaviour 
{
    public Material BoneMaterial;
    public GameObject BodySourceManager;
    
    private Dictionary<ulong, GameObject> _Bodies = new Dictionary<ulong, GameObject>();
    private BodySourceManager _BodyManager;
    private GameObject acqMenustate;
    private MenuState menuState;
    
    private float untrackedTime = 1.0f;

    void Start()
    {
        acqMenustate = GameObject.Find("MenuState");
        menuState = acqMenustate.GetComponent<MenuState>();
    }

    private Dictionary<Kinect.JointType, Kinect.JointType> _BoneMap = new Dictionary<Kinect.JointType, Kinect.JointType>()
    {
        { Kinect.JointType.FootLeft, Kinect.JointType.AnkleLeft },
        { Kinect.JointType.AnkleLeft, Kinect.JointType.KneeLeft },
        { Kinect.JointType.KneeLeft, Kinect.JointType.HipLeft },
        { Kinect.JointType.HipLeft, Kinect.JointType.SpineBase },
        
        { Kinect.JointType.FootRight, Kinect.JointType.AnkleRight },
        { Kinect.JointType.AnkleRight, Kinect.JointType.KneeRight },
        { Kinect.JointType.KneeRight, Kinect.JointType.HipRight },
        { Kinect.JointType.HipRight, Kinect.JointType.SpineBase },
        
        { Kinect.JointType.HandTipLeft, Kinect.JointType.HandLeft },
        { Kinect.JointType.ThumbLeft, Kinect.JointType.HandLeft },
        { Kinect.JointType.HandLeft, Kinect.JointType.WristLeft },
        { Kinect.JointType.WristLeft, Kinect.JointType.ElbowLeft },
        { Kinect.JointType.ElbowLeft, Kinect.JointType.ShoulderLeft },
        { Kinect.JointType.ShoulderLeft, Kinect.JointType.SpineShoulder },
        
        { Kinect.JointType.HandTipRight, Kinect.JointType.HandRight },
        { Kinect.JointType.ThumbRight, Kinect.JointType.HandRight },
        { Kinect.JointType.HandRight, Kinect.JointType.WristRight },
        { Kinect.JointType.WristRight, Kinect.JointType.ElbowRight },
        { Kinect.JointType.ElbowRight, Kinect.JointType.ShoulderRight },
        { Kinect.JointType.ShoulderRight, Kinect.JointType.SpineShoulder },
        
        { Kinect.JointType.SpineBase, Kinect.JointType.SpineMid },
        { Kinect.JointType.SpineMid, Kinect.JointType.SpineShoulder },
        { Kinect.JointType.SpineShoulder, Kinect.JointType.Neck },
        { Kinect.JointType.Neck, Kinect.JointType.Head },
    };
    
    void Update () 
    {
        if (BodySourceManager == null)
        {
            return;
        }
        
        _BodyManager = BodySourceManager.GetComponent<BodySourceManager>();
        if (_BodyManager == null)
        {
            return;
        }
        
        Kinect.Body[] data = _BodyManager.GetData();
        if (data == null)
        {
            return;
        }
        
        List<ulong> trackedIds = new List<ulong>();
        foreach(var body in data)
        {
            if (body == null)
            {
                continue;
              }
                
            if(body.IsTracked)
            {
                trackedIds.Add (body.TrackingId);
            }
        }
        
        List<ulong> knownIds = new List<ulong>(_Bodies.Keys);
        
        // First delete untracked bodies
        foreach(ulong trackingId in knownIds)
        {
            if(!trackedIds.Contains(trackingId))
            {
                Destroy(_Bodies[trackingId]);
                _Bodies.Remove(trackingId);
            }
        }

        foreach(var body in data)
        {
            if (body == null)
            {
                continue;
            }
            
            if(body.IsTracked)
            {
                if(!_Bodies.ContainsKey(body.TrackingId))
                {
                    _Bodies[body.TrackingId] = CreateBodyObject(body.TrackingId);
                }
                menuState.bodyIsTracked = true;
                untrackedTime = 1.0f;
                
                RefreshBodyObject(body, _Bodies[body.TrackingId]);
                PunchCheck(body, _Bodies[body.TrackingId]);
                ArmBinding(body, _Bodies[body.TrackingId]);
                //Debug.Log("body is visible");
            }
            untrackedTime -= Time.deltaTime;
            if (!body.IsTracked && untrackedTime <= 0)
            {
                menuState.bodyIsTracked = false;
                //Debug.Log("body is not visible");
            }
        }
        BoolCountDown();
        
    }

    private Vector3 lastPositionRight;
    private Vector3 lastPositionLeft;
    private Vector3 lastPositionHead;
    private Vector3 lastPositionRightLeg;
    private Vector3 lastPositionLeftLeg;
    private float deltaTimer = 0.3f;
    public bool upperLeftPunch = false;
    public bool upperRightPunch = false;
    public bool lowerLeftPunch = false;
    public bool lowerRightPunch = false;
    public bool headbutt = false;
    public bool leftStep = false;
    public bool rightStep = false;
    public bool leftKick = false;
    public bool rightKick = false;
    private float boolTimer = 0.3f;
    private float attackRange;
    private float attackHeight;
    private float walkHeight;
    private float playerHeightTimer = 5.0f;
    //private bool checkPlayerHeight;
    private float playerHeight;

    private void BoolCountDown()
    {
        if (upperLeftPunch == true || upperRightPunch == true || headbutt == true || leftStep == true || rightStep == true || lowerLeftPunch == true || lowerRightPunch == true || leftKick == true  || rightKick == true)
        {
            boolTimer -= Time.deltaTime;
            if (boolTimer <= 0)
            {
                boolTimer = 0.3f;
                upperLeftPunch = false;
                upperRightPunch = false;
                headbutt = false;
                leftStep = false;
                rightStep = false;
                lowerLeftPunch = false;
                lowerRightPunch = false;
                leftKick = false;
                rightKick = false;
            }
        }
    }

    private void PunchCheck(Kinect.Body body, GameObject bodyObject)
    {
        deltaTimer -= Time.deltaTime;
        Kinect.JointType rightHand = Kinect.JointType.HandRight;
        Kinect.JointType leftHand = Kinect.JointType.HandLeft;
        Kinect.JointType head = Kinect.JointType.Head;
        Kinect.JointType rightLeg = Kinect.JointType.AnkleRight;
        Kinect.JointType leftLeg = Kinect.JointType.AnkleLeft;
        //check the difference in rightHands x-position under a certain timeframe and give a response if it happens
        

        Transform jointObjRight = bodyObject.transform.FindChild(rightHand.ToString());
        Transform jointObjLeft = bodyObject.transform.FindChild(leftHand.ToString());
        Transform jointObjHead = bodyObject.transform.FindChild(head.ToString());
        Transform jointObjRightLeg = bodyObject.transform.FindChild(rightLeg.ToString());
        Transform jointObjLeftLeg = bodyObject.transform.FindChild(leftLeg.ToString());
        //Debug.Log(jointObj.localPosition.x);

        
        if (!menuState.kinectConfig)
        {
            //checkPlayerHeight = false;
            playerHeight = jointObjHead.position.y - jointObjRightLeg.position.y;
            attackRange = playerHeight*0.15f;
            attackHeight = playerHeight*0.9f;
            walkHeight = playerHeight*0.03f;
            //Debug.Log(playerHeight);

        }

        //if (!checkPlayerHeight)
        //{
        //    playerHeightTimer -= Time.deltaTime;
        //    if (playerHeightTimer <= 0)
        //    {
        //        playerHeightTimer = 5.0f;
        //        checkPlayerHeight = true;
        //    }
        //}



        if (deltaTimer <= 0)
        {
            lastPositionRight = jointObjRight.localPosition;
            lastPositionLeft = jointObjLeft.localPosition;
            lastPositionHead = jointObjHead.localPosition;
            lastPositionLeftLeg = jointObjLeftLeg.localPosition;
            lastPositionRightLeg = jointObjRightLeg.localPosition;
            deltaTimer = 0.3f;
        }
        if (jointObjRight.localPosition.z < lastPositionRight.z - attackRange && jointObjRight.localPosition.y >= attackHeight)
        {
            //Debug.Log("UPPER RIGHT PUNCH");
            upperRightPunch = true;
        }
        if (jointObjLeft.localPosition.z < lastPositionLeft.z - attackRange && jointObjLeft.localPosition.y >= attackHeight)
        {
            //Debug.Log("UPPER LEFT PUNCH");
            upperLeftPunch = true;
        }
        if (jointObjRight.localPosition.z < lastPositionRight.z - attackRange && jointObjRight.localPosition.y <= attackHeight)
        {
            //Debug.Log("LOWER RIGHT PUNCH");
            lowerRightPunch = true;
        }
        if (jointObjLeft.localPosition.z < lastPositionLeft.z - attackRange && jointObjLeft.localPosition.y <= attackHeight)
        {
            //Debug.Log("LOWER LEFT PUNCH");
            lowerLeftPunch = true;
        }
        if (jointObjHead.localPosition.z < lastPositionHead.z - 2.5f)
        {
            //Debug.Log("HEADBUTT");
            headbutt = true;
        }
        if (jointObjRightLeg.localPosition.y < lastPositionRightLeg.y - walkHeight)
        {
            //Debug.Log("RIGHTSTEP");
            rightStep = true;
        }
        if (jointObjLeftLeg.localPosition.y < lastPositionLeftLeg.y - walkHeight)
        {
            //Debug.Log("LEFTSTEP");
            leftStep = true;
        }

        if (jointObjLeftLeg.localPosition.z < lastPositionLeftLeg.z - attackRange)
        {
            leftKick = true;
        }
        if (jointObjRightLeg.localPosition.z < lastPositionRightLeg.z - attackRange)
        {
            rightKick = true;
        }

    }


    public Vector3 rightHandWristPos;
    public Vector3 rightWristElbowPos;
    public Vector3 rightElbowShoulderPos;
    public Vector3 rightHandRota;
    public Vector3 rightWristRota;
    public Vector3 rightElbowRota;
    public Vector3 rightShoulderRota;
    public Vector3 leftHandWristPos;
    public Vector3 leftWristElbowPos;
    public Vector3 leftElbowShoulderPos;
    public Vector3 leftHandRota;
    public Vector3 leftWristRota;
    public Vector3 leftElbowRota;
    public Vector3 leftShoulderRota;


    private void ArmBinding(Kinect.Body body, GameObject bodyObject)
    {
        Kinect.JointType rightHand = Kinect.JointType.HandRight;
        Kinect.JointType leftHand = Kinect.JointType.HandLeft;
        Kinect.JointType rightWrist = Kinect.JointType.WristRight;

        Kinect.JointType leftWrist = Kinect.JointType.WristLeft;

        Kinect.JointType rightElbow = Kinect.JointType.ElbowRight;
        Kinect.JointType leftElbow = Kinect.JointType.ElbowLeft;
        Kinect.JointType rightShoulder = Kinect.JointType.ShoulderRight;
        Kinect.JointType leftShoulder = Kinect.JointType.ShoulderLeft;


        // draw the position of the hands and elbows and shit on the joint position and on the line between them


        //TODO: bind an image between each joint

        Transform jointRightHand = bodyObject.transform.FindChild(rightHand.ToString());
        Transform jointRightWrist = bodyObject.transform.FindChild(rightWrist.ToString());
        Transform jointRightElbow = bodyObject.transform.FindChild(rightElbow.ToString());
        Transform jointRightShoulder = bodyObject.transform.FindChild(rightShoulder.ToString());
        Transform jointLeftHand = bodyObject.transform.FindChild(leftHand.ToString());
        Transform jointLeftWrist = bodyObject.transform.FindChild(leftWrist.ToString());
        Transform jointleftElbow = bodyObject.transform.FindChild(leftElbow.ToString());
        Transform jointLeftShoulder = bodyObject.transform.FindChild(leftShoulder.ToString());


        rightHandWristPos = (jointRightHand.localPosition + jointRightWrist.localPosition) /2;
        rightWristElbowPos = (jointRightWrist.localPosition + jointRightElbow.localPosition)/2;
        rightElbowShoulderPos = (jointRightElbow.localPosition + jointRightShoulder.localPosition)/2;
        leftHandWristPos = (jointLeftHand.localPosition + jointLeftWrist.localPosition)/2;
        leftWristElbowPos = (jointLeftWrist.localPosition + jointleftElbow.localPosition)/2;
        leftElbowShoulderPos = (jointleftElbow.localPosition + jointLeftShoulder.localPosition)/2;

        rightHandRota = jointRightHand.localPosition;
        rightWristRota = jointRightWrist.localPosition;
        rightElbowRota = jointRightElbow.localPosition;
        rightShoulderRota = jointRightShoulder.localPosition;
        leftHandRota = jointLeftHand.localPosition;
        leftWristRota = jointLeftWrist.localPosition;
        leftElbowRota = jointleftElbow.localPosition;
        leftShoulderRota = jointLeftShoulder.localPosition;

        //rightHandWristRota = (jointRightHand.localRotation.eulerAngles + jointRightWrist.localRotation.eulerAngles)/2;
        //rightWristElbowRota = (jointRightWrist.localRotation.eulerAngles + jointRightElbow.localRotation.eulerAngles)/2;
        //rightElbowShoulderRota = (jointRightElbow.localRotation.eulerAngles + jointRightShoulder.localRotation.eulerAngles)/2;
        //leftHandWristRota = (jointLeftHand.localRotation.eulerAngles + jointLeftWrist.localRotation.eulerAngles)/2;
        //leftWristElbowRota = (jointLeftWrist.localRotation.eulerAngles + jointleftElbow.localRotation.eulerAngles)/2;
        //leftElbowShoulderRota = (jointleftElbow.localRotation.eulerAngles + jointLeftShoulder.localRotation.eulerAngles)/2;
        //SetFromToRotation(jointRightHand.localPosition, jointRightWrist.localPosition) ;

        //push out the transforms so that i can do SetFromToRotation in the other script

    }

    private GameObject CreateBodyObject(ulong id)
    {
        GameObject body = new GameObject("Body:" + id);
        
        for (Kinect.JointType jt = Kinect.JointType.SpineBase; jt <= Kinect.JointType.ThumbRight; jt++)
        {
            GameObject jointObj = GameObject.CreatePrimitive(PrimitiveType.Cube);

            LineRenderer lr = jointObj.AddComponent<LineRenderer>();
            lr.SetVertexCount(2);
            lr.material = BoneMaterial;
            lr.SetWidth(0.05f, 0.05f);

            jointObj.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
            jointObj.name = jt.ToString();
            jointObj.transform.parent = body.transform;
        }
        
        return body;
    }
    
    private void RefreshBodyObject(Kinect.Body body, GameObject bodyObject)
    {
        for (Kinect.JointType jt = Kinect.JointType.SpineBase; jt <= Kinect.JointType.ThumbRight; jt++)
        {
            Kinect.Joint sourceJoint = body.Joints[jt];
            Kinect.Joint? targetJoint = null;
            
            if(_BoneMap.ContainsKey(jt))
            {
                targetJoint = body.Joints[_BoneMap[jt]];
            }
            
            Transform jointObj = bodyObject.transform.FindChild(jt.ToString());
            jointObj.localPosition = GetVector3FromJoint(sourceJoint);


            LineRenderer lr = jointObj.GetComponent<LineRenderer>();
            if (targetJoint.HasValue)
            {
                lr.SetPosition(0, jointObj.localPosition);
                lr.SetPosition(1, GetVector3FromJoint(targetJoint.Value));
                lr.SetColors(GetColorForState(sourceJoint.TrackingState), GetColorForState(targetJoint.Value.TrackingState));
            }
            else
            {
                lr.enabled = false;
            }
        }
    }
    
    private static Color GetColorForState(Kinect.TrackingState state)
    {
        switch (state)
        {
        case Kinect.TrackingState.Tracked:
            return Color.green;

        case Kinect.TrackingState.Inferred:
            return Color.red;

        default:
            return Color.black;
        }
    }
    
    private static Vector3 GetVector3FromJoint(Kinect.Joint joint)
    {
        return new Vector3(joint.Position.X * 10 + 40, joint.Position.Y * 10, joint.Position.Z * 10);
    }
}
