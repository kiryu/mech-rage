﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BuildingBehaviour_ver2 : MonoBehaviour
{


    //public Text hitText;

    private Vector3 lastPosition;
    private GameObject acqBuildingManager;
    private BuildingManager buildingManager;
    private int health;
    private bool isActive;

    private GameObject acqRageBar;
    private RageBehaviour rageBehaviour;

    private GameObject puncheffect;
    GameObject punchEffectManager;

    private GameObject acqScorePopup;
    private PopupText popupText;
    private Vector3 fontPosition;


    public enum DAMAGETYPE
    {
        NODAMAGE,
        DAMAGED,
        DESTROYED
    }

    public DAMAGETYPE myDamage;
    //public DAMAGETYPE currentDamage;

    // Use this for initialization
    void Start()
    {
        puncheffect = GameObject.Find("PunchEffectManager");
        
        acqBuildingManager = GameObject.Find("BuildingManager");
        //transform.position = new Vector3(0,0,0);
        lastPosition = transform.position;
        buildingManager = acqBuildingManager.GetComponent<BuildingManager>();
        health = 3;
        isActive = true;
        myDamage = DAMAGETYPE.NODAMAGE;

        acqRageBar = GameObject.Find("RageBehaviour");
        rageBehaviour = acqRageBar.GetComponent<RageBehaviour>();

        acqScorePopup = GameObject.Find("ScorePopup");
        popupText = acqScorePopup.GetComponent<PopupText>();
        fontPosition = new Vector3(0, 100);


    }

    // Update is called once per frame
    void Update ()
    {

        if (isActive == false)
        {
            health = 3;
            myDamage = DAMAGETYPE.NODAMAGE;
            isActive = true;
            Debug.Log("active");
        }

        //    lastPosition = transform.position;
        //    if (buildingManager.isMoving)
        //    {
        //        transform.position = Vector3.Lerp(lastPosition, lastPosition + buildingManager.distance,
        //            Mathf.Min(buildingManager.deltaTimer*buildingManager.currentSpeed, 1));
        //        //Debug.Log(buildingManager.currentSpeed);

        //    }
        //    if (!buildingManager.isMoving)
        //    {
        //        lastPosition = transform.position;
        //    }
        //transform.localScale = new Vector3(transform.position.z * 0.008f, transform.position.z * 0.008f,
        //    transform.position.z * 0.008f);

        if (myDamage != DAMAGETYPE.DAMAGED)
        {
            if (health <= 0)
            {

                myDamage = DAMAGETYPE.DESTROYED;

                buildingManager.buildingsDestroyed--;
                isActive = false;
                gameObject.SetActive(false);

            }
        }
        if (isHit)
        {
            feedCounter -= Time.deltaTime;
            if (feedCounter <= 0)
            {

                //hitText.text = "";
                isHit = false;
                feedCounter = 1f;
            }
        }
        if (myDamage == DAMAGETYPE.DESTROYED)
        {
            buildingManager.isCollided = false;
            myDamage = DAMAGETYPE.DAMAGED;
            Debug.Log("Destroyed");
        }

    }

    private float feedCounter = 1f;
    private bool isHit = false;
    

    void OnTriggerEnter(Collider coll)
    {
        //Debug.Log("Collision");
        if (coll.gameObject.tag == "AttackBox")
        {

            if (!isHit)
            {
                if (coll.gameObject.GetComponent<AttackBehaviour_ver2>().attackType ==
                    AttackBehaviour_ver2.ATTACKTYPE.PUNCH ||
                    coll.gameObject.GetComponent<AttackBehaviour_ver2>().attackType ==
                    AttackBehaviour_ver2.ATTACKTYPE.KICK)
                {
                    health -= 1;
                    ScoreBarManager.score += 20;
                    coll.gameObject.GetComponent<MultipleSounds>().audioClip1.pitch = Random.Range(0.8f, 1.2f);
                    coll.gameObject.GetComponent<MultipleSounds>().audioClip1.Play();
                    rageBehaviour.rage += 10;
                    popupText.ScorePop(20, transform.position + fontPosition, new Vector3(0, 5));
                }
                if (coll.gameObject.GetComponent<AttackBehaviour_ver2>().attackType == AttackBehaviour_ver2.ATTACKTYPE.RAGE)
                {
                    health -= 3;
                    coll.gameObject.GetComponent<MultipleSounds>().audioClip1.pitch = Random.Range(0.8f, 1.2f);
                    coll.gameObject.GetComponent<MultipleSounds>().audioClip1.Play();//replace with rage destruction sound effect
                    ScoreBarManager.score += 200;
                    popupText.ScorePop(200, transform.position + fontPosition, new Vector3(0, 5));
                }
                Debug.Log(health);
                //Debug.Log("hit");
                //hitText.text = "Hit";
                punchEffectManager = acqBuildingManager.GetComponent<CommonObjectPoolerScript>().GetPooledObject();
                if (punchEffectManager == null)
                {
                    return;
                }
                punchEffectManager.transform.position = (transform.position + new Vector3(2, 14, 2));
                punchEffectManager.SetActive(true);
                punchEffectManager.GetComponent<ParticleSystem>().Play();
                isHit = true;
            }
            
            //buildingManager.isCollided = false;
            //gameObject.SetActive(false);

            
            //health -= 1;
            //Debug.Log(health);
            //Debug.Log("hit");
            //hitText.text = "Hit";
            //isHit = true;
            //buildingManager.isCollided = false;
            //gameObject.SetActive(false);

        }
        if (coll.gameObject.tag == "Player")
        {
            if (myDamage != DAMAGETYPE.DESTROYED)
            {
                buildingManager.isCollided = true;
                Debug.Log("Crash");
            }

        }

    }
}
