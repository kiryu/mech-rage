﻿using UnityEngine;
using System.Collections;

public class StaticBuildingBehaviour : MonoBehaviour
{
    private GameObject acqBuildingManager;
    
    private BuildingManager buildingManager;
    private Vector3 lastPosition;
    private Vector3 resetPosition;

    public enum SEGMENTTYPE
    {
        BEACH,
        BUILDINGS
    }

    public SEGMENTTYPE myType;

    //public enum SEGMENTS
    //{
    //    SEGMENT1,
    //    SEGMENT2,
    //    SEGMENT3
    //}

    //public SEGMENTS mySegment;

    // Use this for initialization
	void Start ()
    {
	acqBuildingManager = GameObject.Find("BuildingManager");
        
	    buildingManager = acqBuildingManager.GetComponent<BuildingManager>();
	    lastPosition = transform.position;
        resetPosition = new Vector3(76.4f, -166.4f, -292.9f);
    }
	
	// Update is called once per frame
	void Update ()
    {
        lastPosition = transform.position;
        if (buildingManager.isMoving)
        {
            transform.position = Vector3.Lerp(lastPosition, lastPosition + buildingManager.distance,
                Mathf.Min(buildingManager.deltaTimer * (buildingManager.currentSpeed * 2), 1));
            //Debug.Log(buildingManager.currentSpeed);

        }
        if (!buildingManager.isMoving)
        {
            lastPosition = transform.position;
        }




	    if (transform.localPosition.z >= 250)
	    {
	        if (myType == SEGMENTTYPE.BEACH)
	        {
	            gameObject.SetActive(false);
	        }
	        if (myType == SEGMENTTYPE.BUILDINGS)
	        {

	            transform.localPosition = resetPosition;
	            lastPosition = transform.position;

	            gameObject.SetActive(false);

	        }
	    }
    }
}
