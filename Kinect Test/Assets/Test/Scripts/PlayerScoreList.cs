﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerScoreList : MonoBehaviour {

    public GameObject playerScoreEntryPrefab;

    ScoreManager scoreManager;
	// Use this for initialization
	void Start () {
        scoreManager = GameObject.FindObjectOfType<ScoreManager>();

        
	}
	
	// Update is called once per frame
	void Update () {
        if (scoreManager == null)

        {
            Debug.Log("You need to assign the scoreManager script to an empty object called ScoreManager in the game");
            return;
        }
        while (this.transform.childCount<0)
        {
            Transform c = this.transform.GetChild(0);
            c.SetParent(null);
            Destroy(c.gameObject);
        }
        string[] names = scoreManager.GetPlayerNames();

        foreach (string name in names)
        {
            GameObject go = (GameObject)Instantiate(playerScoreEntryPrefab);
            go.transform.SetParent(this.transform);
            go.transform.Find("Username").GetComponent<Text>().text = name;
            go.transform.Find("Score").GetComponent<Text>().text =name;
        }
    }
}
