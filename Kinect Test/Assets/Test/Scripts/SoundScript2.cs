﻿using UnityEngine;
using System.Collections;

public class SoundScript2 : MonoBehaviour {

	// Use this for initialization
    private float soundPitch = 1;
    private float soundPitchRand = 0.20f;
    private bool startCondition = false;
    // Use this for initialization
    void Start()
    {
        gameObject.GetComponent<AudioSource>();

    }

    // Update is called once per frame
    void Update()
    {
        if (startCondition == true)
        {
            playAudio();
        }
    }

    void playAudio()
    {

        startCondition = false;
        gameObject.GetComponent<AudioSource>().pitch = Random.Range(1.0f - soundPitchRand, 1.0f + soundPitchRand); //Randomizes pitch
        gameObject.GetComponent<AudioSource>().Play(); //Plays the attached Audio Source

    }
}
