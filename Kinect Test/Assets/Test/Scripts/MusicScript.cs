﻿using UnityEngine;
using System.Collections;

public class MusicScript : MonoBehaviour
{
    private GameObject acqMenu;
    private StateManager menu;
    private bool musicIsPlaying;

	// Use this for initialization
	void Start () {
        acqMenu = GameObject.Find("StateManager");
	    menu = acqMenu.GetComponent<StateManager>();
	    musicIsPlaying = false;
	}
	
	// Update is called once per frame
	void Update () {
	    if (menu.CurrentState == StateManager.STATE.GAMESTATE && !musicIsPlaying)
	    {
	        gameObject.GetComponent<AudioSource>().Play();
	        musicIsPlaying = true;
	    }
	}
}
