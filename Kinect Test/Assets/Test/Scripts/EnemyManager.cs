﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EnemyManager : MonoBehaviour
{
    public Text text;
    public int enemiesLeft;

	// Use this for initialization
	void Start ()
	{
	    text.text = "";
	    enemiesLeft = 5;
	}
	
	// Update is called once per frame
	void Update ()
    {
	    if (enemiesLeft == 0)
	    {
	        text.text = "YOU WIN";
	    }
	
	}
}
