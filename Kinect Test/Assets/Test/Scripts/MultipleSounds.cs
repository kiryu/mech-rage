﻿using UnityEngine;
using System.Collections;

public class MultipleSounds : MonoBehaviour
{

    public AudioClip Clip1;
    public bool clip1Loop;
    public bool clip1PlayOnAwake;
    public float clip1Volume;
    public AudioSource audioClip1;

    public AudioClip Clip2;
    public bool clip2Loop;
    public bool clip2PlayOnAwake;
    public float clip2Volume;
    public AudioSource audioClip2;

    public AudioClip Clip3;
    public bool clip3Loop;
    public bool clip3PlayOnAwake;
    public float clip3Volume;
    public AudioSource audioClip3;

    public AudioClip Clip4;
    public bool clip4Loop;
    public bool clip4PlayOnAwake;
    public float clip4Volume;
    public AudioSource audioClip4;

    public AudioClip Clip5;
    public bool clip5Loop;
    public bool clip5PlayOnAwake;
    public float clip5Volume;
    public AudioSource audioClip5;

    public AudioSource AddAudio(AudioClip clip, bool loop, bool playAwake, float vol)
    {
        AudioSource newAudio = gameObject.AddComponent<AudioSource>();
        newAudio.clip = clip;
        newAudio.loop = loop;
        newAudio.playOnAwake = playAwake;
        newAudio.volume = vol;


        return newAudio;
    }

    public void Awake()
    {
        audioClip1 = AddAudio(Clip1, clip1Loop, clip1PlayOnAwake, clip1Volume);
        audioClip2 = AddAudio(Clip2, clip2Loop, clip2PlayOnAwake, clip2Volume);
        audioClip3 = AddAudio(Clip3, clip3Loop, clip3PlayOnAwake, clip3Volume);
        audioClip4 = AddAudio(Clip4, clip4Loop, clip4PlayOnAwake, clip4Volume);
        audioClip5 = AddAudio(Clip5, clip5Loop, clip5PlayOnAwake, clip5Volume);
    }

    // Use this for initialization
	void Start ()
    {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
	
	}
}
