﻿using UnityEngine;
using System.Collections;

public class MenuState : MonoBehaviour {

    public bool kinectConfig;
    public bool bodyIsTracked;
    
    private float configTime;
    private BodySourceView bodySource;
    private GameObject kinectBody;
    private GameObject acqStateManager;
    private StateManager stateManager;
    private GameObject acqCamera;
    private Camera camera;
    private Vector3 lastCameraPosition;
    private int wallHealth;

    // Use this for initialization
    void Start ()
    {
        kinectBody = GameObject.Find("BodyView");
        bodySource = kinectBody.GetComponent<BodySourceView>();
        acqStateManager = GameObject.Find("StateManager");
        stateManager = acqStateManager.GetComponent<StateManager>();
        wallHealth = 5;
        acqCamera = GameObject.Find("Main Camera");
        camera = acqCamera.GetComponent<Camera>();
        lastCameraPosition = camera.transform.position;
        camera.transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z + 50);
        kinectConfig = false;
        configTime = 10.0f;
        
    }
	
	// Update is called once per frame
	void Update ()
    {

        if (!kinectConfig)
        {
            if (bodyIsTracked)
            {
                Debug.Log("Body is tracked");
                configTime -= Time.deltaTime;
                if (configTime <= 0)
                {
                    kinectConfig = true;
                }
            }

            if (Input.GetKeyDown(KeyCode.KeypadPlus))
            {
                kinectConfig = true;
            }
        }
        
	    if (!bodyIsTracked)
	    {
            configTime = 10.0f;
        }
        //punch the screen three times to transition into the game
        //also move the camera and arms on start to somewhere else so that the menu is visible
        if (stateManager.CurrentState == StateManager.STATE.MENUSTATE && kinectConfig)
	    {
	        if (bodySource.lowerLeftPunch || bodySource.lowerRightPunch || bodySource.upperRightPunch || bodySource.upperLeftPunch)
	        {
	            wallHealth -= 1;
	        }

	        if (Input.GetKeyDown(KeyCode.I) || Input.GetKeyDown(KeyCode.U) || Input.GetKeyDown(KeyCode.J) || Input.GetKeyDown(KeyCode.K))
	        {
                wallHealth -= 1;
            }

	        if (wallHealth == 0)
	        {
	            camera.transform.position = lastCameraPosition;
	            stateManager.CurrentState = StateManager.STATE.GAMESTATE;
                Debug.Log("state change");
	        }
	    }
	}
}
