﻿using UnityEngine;
using System.Collections;

public class AttackBehaviour_ver2 : MonoBehaviour
{
    private GameObject thePlayer;
    private GameObject kinectBody;
    private GameObject acqBuildingManager;
    private GameObject rageParticle;
    private BuildingManager buildingManager;
    private Vector3 relativePosition;
    private BodySourceView bodySource;
    public bool isAttacking;
    private float attackTimer;
    private MultipleSounds multipleSounds;
    private GameObject acqRageBar;
    private RageBehaviour rageBehaviour;

    public enum ATTACKTYPE
    {
        PUNCH,
        KICK,
        RAGE
    }

    public ATTACKTYPE attackType;

	// Use this for initialization
	void Start ()
    {
	thePlayer = GameObject.Find("Player");
        kinectBody = GameObject.Find("BodyView");
	    bodySource = kinectBody.GetComponent<BodySourceView>();
        acqBuildingManager = GameObject.Find("BuildingManager");
	    buildingManager = acqBuildingManager.GetComponent<BuildingManager>();
        relativePosition = new Vector2(10000,10000);
	    attackTimer = 1;
	    isAttacking = false;
	    multipleSounds = gameObject.GetComponent<MultipleSounds>();
        acqRageBar = GameObject.Find("RageBehaviour");
        rageBehaviour = acqRageBar.GetComponent<RageBehaviour>();
        rageParticle = GameObject.Find("SpecialAttackFX");
        
    }

    //learn Awake functions
	
	// Update is called once per frame
	void Update ()
    {
	    if (!buildingManager.controlType)
	    {


	        if (!isAttacking)
	        {
	            //*/
	            if (Input.GetKeyDown(KeyCode.K))
	                /*/	        
            if (bodySource.lowerRightPunch)
            //*/
	            {
	                isAttacking = true;
	                relativePosition = new Vector3(thePlayer.transform.position.x - 2, thePlayer.transform.position.y,
	                    thePlayer.transform.position.z + 10);
	                attackType = ATTACKTYPE.PUNCH;
	                //Debug.Log("K");
	            }
	            //*/
	            if (Input.GetKeyDown(KeyCode.J))
	                /*/
	        if (bodySource.lowerLeftPunch)
            //*/
	            {
	                isAttacking = true;
	                relativePosition = new Vector3(thePlayer.transform.position.x + 2, thePlayer.transform.position.y,
	                    thePlayer.transform.position.z + 10);
	                attackType = ATTACKTYPE.PUNCH;
	                //Debug.Log("J");
	            }
	            //*/
	            if (Input.GetKeyDown(KeyCode.I))
	                /*/
	        if (bodySource.upperRightPunch)
            //*/
	            {
	                isAttacking = true;
	                relativePosition = new Vector3(thePlayer.transform.position.x - 2, thePlayer.transform.position.y + 2,
	                    thePlayer.transform.position.z + 10);
	                attackType = ATTACKTYPE.PUNCH;
	                //Debug.Log("I");
	            }
	            //*/
	            if (Input.GetKeyDown(KeyCode.U))
	                /*/
	        if (bodySource.upperLeftPunch)
            //*/
	            {
	                isAttacking = true;
	                relativePosition = new Vector3(thePlayer.transform.position.x + 2, thePlayer.transform.position.y + 2,
	                    thePlayer.transform.position.z + 10);
	                attackType = ATTACKTYPE.PUNCH;
	                //Debug.Log("U");
	            }
	            //*/
	            if (Input.GetKeyDown(KeyCode.Space))
	                /*/
	        if (bodySource.headbutt)
            //*/
	            {
	                isAttacking = true;
	                relativePosition = new Vector3(thePlayer.transform.position.x, thePlayer.transform.position.y,
	                    thePlayer.transform.position.z + 10);
	                attackType = ATTACKTYPE.PUNCH;
	            }
	            //*/
	            if (Input.GetKeyDown(KeyCode.B))
	                /*/
            if (bodySource.leftKick)
            //*/
	            {
	                isAttacking = true;
	                relativePosition = new Vector3(thePlayer.transform.position.x + 2, thePlayer.transform.position.y - 3,
	                    thePlayer.transform.position.z + 10);
	                attackType = ATTACKTYPE.KICK;
	            }

	            //*/
	            if (Input.GetKeyDown(KeyCode.N))
	                /*/
            if (bodySource.rightKick)
            //*/
	            {
	                isAttacking = true;
	                relativePosition = new Vector3(thePlayer.transform.position.x - 1, thePlayer.transform.position.y - 3,
	                    thePlayer.transform.position.z + 10);
	                attackType = ATTACKTYPE.KICK;
	            }

	            if (rageBehaviour.rage >= 50)
	            {


	                if (  Input.GetKeyDown(KeyCode.U) && 
	                    Input.GetKeyDown(KeyCode.I))
	                {
	                    isAttacking = true;
	                    relativePosition = new Vector3(thePlayer.transform.position.x + 0.5f, thePlayer.transform.position.y + 2,
	                        thePlayer.transform.position.z + 10);
	                    attackType = ATTACKTYPE.RAGE;
	                    rageBehaviour.rage -= 50;
                        rageParticle.transform.position = new Vector3(relativePosition.x, relativePosition.y - 2);
                        rageParticle.GetComponent<ParticleSystem>().Play();
	                    //create partical effect here
	                }

	                if (Input.GetKeyDown(KeyCode.J) && Input.GetKeyDown(KeyCode.K) )
	                {
                        isAttacking = true;
                        relativePosition = new Vector3(thePlayer.transform.position.x + 0.5f, thePlayer.transform.position.y,
                            thePlayer.transform.position.z + 10);
                        attackType = ATTACKTYPE.RAGE;
                        rageBehaviour.rage -= 50;
                        rageParticle.transform.position = new Vector3(relativePosition.x, relativePosition.y - 2);
                        rageParticle.GetComponent<ParticleSystem>().Play();
                    }
	            }

	        }
	    }

	    if (buildingManager.controlType)
	    {
            if (!isAttacking)
            {
                /*/
                if (Input.GetKeyDown(KeyCode.K))
                /*/	        
        if (bodySource.lowerRightPunch)
        //*/
                {
                    isAttacking = true;
                    relativePosition = new Vector3(thePlayer.transform.position.x - 2, thePlayer.transform.position.y,
                        thePlayer.transform.position.z + 10);
                    attackType = ATTACKTYPE.PUNCH;
                    //Debug.Log("low");
                }
                /*/
                if (Input.GetKeyDown(KeyCode.J))
                /*/
        if (bodySource.lowerLeftPunch)
        //*/
                {
                    isAttacking = true;
                    relativePosition = new Vector3(thePlayer.transform.position.x + 2, thePlayer.transform.position.y,
                        thePlayer.transform.position.z + 10);
                    attackType = ATTACKTYPE.PUNCH;
                    //Debug.Log("low");
                }
                /*/
                if (Input.GetKeyDown(KeyCode.I))
                /*/
        if (bodySource.upperRightPunch)
        //*/
                {
                    isAttacking = true;
                    relativePosition = new Vector3(thePlayer.transform.position.x - 2, thePlayer.transform.position.y + 2,
                        thePlayer.transform.position.z + 10);
                    attackType = ATTACKTYPE.PUNCH;
                    //Debug.Log("high");
                }
                /*/
                if (Input.GetKeyDown(KeyCode.U))
                /*/
        if (bodySource.upperLeftPunch)
        //*/
                {
                    isAttacking = true;
                    relativePosition = new Vector3(thePlayer.transform.position.x + 2, thePlayer.transform.position.y + 2,
                        thePlayer.transform.position.z + 10);
                    attackType = ATTACKTYPE.PUNCH;
                    //Debug.Log("high");
                }
                /*/
                if (Input.GetKeyDown(KeyCode.Space))
                /*/
        if (bodySource.headbutt)
        //*/
                {
                    isAttacking = true;
                    relativePosition = new Vector3(thePlayer.transform.position.x, thePlayer.transform.position.y,
                        thePlayer.transform.position.z + 10);
                    attackType = ATTACKTYPE.PUNCH;
                }
                /*/
                if (Input.GetKeyDown(KeyCode.B))
                /*/
        if (bodySource.leftKick)
        //*/
                {
                    isAttacking = true;
                    relativePosition = new Vector3(thePlayer.transform.position.x + 2, thePlayer.transform.position.y - 3,
                        thePlayer.transform.position.z + 10);
                    attackType = ATTACKTYPE.KICK;
                }

                /*/
                if (Input.GetKeyDown(KeyCode.N))
                /*/
        if (bodySource.rightKick)
        //*/
                {
                    isAttacking = true;
                    relativePosition = new Vector3(thePlayer.transform.position.x - 1, thePlayer.transform.position.y - 3,
                        thePlayer.transform.position.z + 10);
                    attackType = ATTACKTYPE.KICK;
                }

                if (rageBehaviour.rage >= 50)
                {

                    /*/
                    if (bodySource.lowerRightPunch  && bodySource.lowerLeftPunch )
                        /*/
                        if (bodySource.headbutt)
                        //*/
                    {
                        isAttacking = true;
                        relativePosition = new Vector3(thePlayer.transform.position.x + 0.5f, thePlayer.transform.position.y,
                            thePlayer.transform.position.z - 5);
                        attackType = ATTACKTYPE.RAGE;
                        rageBehaviour.rage -= 50;
                        rageParticle.transform.position = new Vector3(relativePosition.x, relativePosition.y - 2);
                        rageParticle.GetComponent<ParticleSystem>().Play();
                        //create partical effect here
                    }

                    //if (bodySource.upperRightPunch && bodySource.upperLeftPunch)
                    //{
                    //    isAttacking = true;
                    //    relativePosition = new Vector3(thePlayer.transform.position.x + 0.5f, thePlayer.transform.position.y + 2,
                    //        thePlayer.transform.position.z + 10);
                    //    attackType = ATTACKTYPE.RAGE;
                    //    rageBehaviour.rage -= 50;
                    //    rageParticle.transform.position = new Vector3(relativePosition.x, relativePosition.y - 2);
                    //    rageParticle.GetComponent<ParticleSystem>().Play();
                    //}
                }

            }
        }

	    if (isAttacking)
	    {
	        attackTimer -= Time.deltaTime;
            transform.position = relativePosition;

	        if (attackType == ATTACKTYPE.KICK)
	        {
	            gameObject.GetComponent<SpriteRenderer>().enabled = true;
	        }

	        if (attackTimer <= 0)
	        {
	            isAttacking = false;
                gameObject.GetComponent<SpriteRenderer>().enabled = false;
                attackTimer = 1;
                transform.position = new Vector3(10000,10000);
	        }
        }
        
    }
}
