﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;

public class BuildingManager : MonoBehaviour
{
    public bool isMoving;
    public float deltaTimer;
    public int currentSpeed;
    public Vector3 distance;
    public float counter;
    public Text countText;
    public bool isCollided;
    public int buildingsDestroyed;
    public Text winText;
    public bool controlType;
    
    private float speed;
    private float movementTimer;
    private bool runCheck;
    private bool firstButtonPressed;
    private float returnValue;
    private float delay;
    private bool isBreaking;
    private BodySourceView bodySource;
    private GameObject kinectBody;
    private GameObject acqStateManager;
    private StateManager stateManager;
    GameObject buildingObject;

	// Use this for initialization
	void Start ()
	{
	    isMoving = false;
	    movementTimer = 0.5f;
	    deltaTimer = 0;
	    speed = 25f;
	    currentSpeed = 0;
	    runCheck = false;
	    firstButtonPressed = false;
	    returnValue = -1;
	    delay = 1;
	    distance = new Vector3(0,0,-speed);
	    isBreaking = false;
	    counter = 0;
        acqStateManager = GameObject.Find("StateManager");
	    stateManager = acqStateManager.GetComponent<StateManager>();
        kinectBody = GameObject.Find("BodyView");
        bodySource = kinectBody.GetComponent<BodySourceView>();
        countText.text = "MovementCounter:" + counter.ToString();
	    winText.text = "";
	    isCollided = false;
	    buildingsDestroyed = 10;
	    controlType = true;
	    
	}
	
	// Update is called once per frame
    void Update()
    {

        


        if (Input.GetKeyDown(KeyCode.KeypadEnter))
        {

            if (!controlType)
            {
                controlType = true;
                Debug.Log("Kinect");
            }
            else
            {
                controlType = false;
                Debug.Log("Keyboard");
            }
        }

        if (stateManager.CurrentState == StateManager.STATE.GAMESTATE)
        {



            if (!controlType)
            {


                if (isBreaking)
                {
                    //*/
                    if (Input.GetKeyDown(KeyCode.J) || Input.GetKeyDown(KeyCode.K) || Input.GetKeyDown(KeyCode.U) ||
                        Input.GetKeyDown(KeyCode.I))
                        /*/
            if (bodySource.lowerLeftPunch || bodySource.lowerRightPunch || bodySource.upperLeftPunch || bodySource.upperRightPunch)
            //*/
                    {
                        isBreaking = false;
                        buildingsDestroyed = 10;
                        //firstBuildingCreated = false;
                        //secondBuildingCreated = false;
                        //thirdBuildingCreated = false;
                        //fourthBuildingCreated = false;
                        //fithBuildingCreated = false;
                        //sixthBuildingCreated = false;
                        //seventhBuildingCreated = false;
                        //eighthBuildingCreated = false;
                        //ninethBuildingCreated = false;
                        //tenthBuildingCreated = false;
                        winText.text = "";
                        counter = 0;
                    }
                }

                if (!isBreaking)
                {
                    if (buildingsDestroyed == 0)
                    {
                        winText.text = "YOU WIN";
                        isMoving = false;
                        isBreaking = true;
                        SceneManager.LoadScene("Score Screen");
                        //create an end scene when the player dies or wins and then have some sort of input that returns them to the main scene
                    }

                    //*/
                    if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.A))
                        /*/
            if (bodySource.leftStep || bodySource.rightStep)
            //*/
                    {



                        if (!runCheck)
                        {
                            //*/
                            if (Input.GetKeyDown(KeyCode.A))
                                /*/
                    if (bodySource.leftStep)
                    //*/
                            {
                                firstButtonPressed = true;
                                runCheck = true;

                            }
                            //*/
                            if (Input.GetKeyDown(KeyCode.D))
                                /*/
                    if (bodySource.rightStep)
                    //*/
                            {
                                firstButtonPressed = false;
                                runCheck = true;
                            }
                        }
                    }
                    if (runCheck && isMoving)
                    {
                        delay -= Time.deltaTime;
                        if (firstButtonPressed)
                        {
                            //*/
                            if (Input.GetKeyDown(KeyCode.D))
                                /*/
                    if (bodySource.rightStep)
                        //*/
                            {
                                runCheck = false;
                                returnValue = delay;
                                delay = 1;
                                //returnValue;

                            }
                        }
                        if (!firstButtonPressed)
                        {
                            //*/
                            if (Input.GetKeyDown(KeyCode.A))
                                /*/
                    if (bodySource.leftStep)
                        //*/
                            {
                                runCheck = false;
                                returnValue = delay;
                                delay = 1;
                                //returnValue;
                            }
                        }
                        if (delay == 0)
                        {
                            runCheck = false;
                            returnValue = delay;
                            delay = 1;
                        }
                    }



                    //*/
                    if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.A))
                        /*/
            if (bodySource.leftStep || bodySource.rightStep)
            //*/
                    {
                        isMoving = true;
                        movementTimer = 0.5f;
                        //currentSpeed++;
                        //speed++;

                        if (currentSpeed >= 5)
                        {
                            currentSpeed = 5;
                        }
                        //speed *= currentSpeed;
                        //TODO:make things slow down somehow when the input intensity is lowered

                        if (returnValue != -1)
                        {
                            if (returnValue >= 0.8f)
                            {
                                //figure out what value buttondelay should have
                                speed = 15f;
                                currentSpeed = 5;
                            }
                            if (returnValue >= 0.6f && returnValue < 0.8f)
                            {
                                //figure out what value buttondelay should have
                                speed = 14f;
                                currentSpeed = 4;
                            }
                            if (returnValue >= 0.4f && returnValue < 0.6f)
                            {
                                //figure out what value buttondelay should have
                                speed = 13f;
                                currentSpeed = 3;
                            }
                            if (returnValue >= 0.2f && returnValue < 0.4f)
                            {
                                //figure out what value buttondelay should have
                                speed = 12f;
                                currentSpeed = 2;
                            }
                            if (returnValue <= 0.2f)
                            {
                                //figure out what value buttondelay should have
                                speed = 11f;
                                currentSpeed = 1;
                            }
                        }
                    }
                    if (isCollided)
                    {
                        isMoving = false;
                        movementTimer = 0.5f;
                        deltaTimer = 0;
                        currentSpeed = 0;
                        speed = 2f;

                    }

                    //Debug.Log(speed);

                    if (!runCheck)
                    {
                        returnValue = 0;
                    }

                    deltaTimer = 0;
                }
            }
            if (controlType)
            {

                if (isBreaking)
                {
                    /*/
                if (Input.GetKeyDown(KeyCode.J) || Input.GetKeyDown(KeyCode.K) || Input.GetKeyDown(KeyCode.U) ||
                    Input.GetKeyDown(KeyCode.I))
                /*/
                    if (bodySource.lowerLeftPunch || bodySource.lowerRightPunch || bodySource.upperLeftPunch ||
                        bodySource.upperRightPunch)
                        //*/
                    {
                        isBreaking = false;
                        buildingsDestroyed = 10;
                        winText.text = "";
                        counter = 0;
                    }
                }

                if (!isBreaking)
                {
                    if (buildingsDestroyed == 0)
                    {
                        winText.text = "YOU WIN";
                        isMoving = false;
                        isBreaking = true;
                    }

                    /*/
                if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.A))
                /*/
                    if (bodySource.leftStep || bodySource.rightStep)
                        //*/
                    {



                        if (!runCheck)
                        {
                            /*/
                        if (Input.GetKeyDown(KeyCode.A))
                        /*/
                            if (bodySource.leftStep)
                                //*/
                            {
                                firstButtonPressed = true;
                                runCheck = true;

                            }
                            /*/
                        if (Input.GetKeyDown(KeyCode.D))
                        /*/
                            if (bodySource.rightStep)
                                //*/
                            {
                                firstButtonPressed = false;
                                runCheck = true;
                            }
                        }
                    }
                    if (runCheck && isMoving)
                    {
                        delay -= Time.deltaTime;
                        if (firstButtonPressed)
                        {
                            /*/
                        if (Input.GetKeyDown(KeyCode.D))
                        /*/
                            if (bodySource.rightStep)
                                //*/
                            {
                                runCheck = false;
                                returnValue = delay;
                                delay = 1;


                            }
                        }
                        if (!firstButtonPressed)
                        {
                            /*/
                        if (Input.GetKeyDown(KeyCode.A))
                        /*/
                            if (bodySource.leftStep)
                                //*/
                            {
                                runCheck = false;
                                returnValue = delay;
                                delay = 1;

                            }
                        }
                        if (delay == 0)
                        {
                            runCheck = false;
                            returnValue = delay;
                            delay = 1;
                        }
                    }



                    /*/
                if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.A))
                /*/
                    if (bodySource.leftStep || bodySource.rightStep)
                        //*/
                    {
                        isMoving = true;
                        movementTimer = 0.5f;


                        if (currentSpeed >= 5)
                        {
                            currentSpeed = 5;
                        }


                        if (returnValue != -1)
                        {
                            if (returnValue >= 0.8f)
                            {

                                speed = 15f;
                                currentSpeed = 5;
                            }
                            if (returnValue >= 0.6f && returnValue < 0.8f)
                            {

                                speed = 14f;
                                currentSpeed = 4;
                            }
                            if (returnValue >= 0.4f && returnValue < 0.6f)
                            {

                                speed = 13f;
                                currentSpeed = 3;
                            }
                            if (returnValue >= 0.2f && returnValue < 0.4f)
                            {

                                speed = 12f;
                                currentSpeed = 2;
                            }
                            if (returnValue <= 0.2f)
                            {

                                speed = 11f;
                                currentSpeed = 1;
                            }
                        }
                    }
                    if (isCollided)
                    {
                        isMoving = false;
                        movementTimer = 0.5f;
                        deltaTimer = 0;
                        currentSpeed = 0;
                        speed = 2f;

                    }



                    if (!runCheck)
                    {
                        returnValue = 0;
                    }

                    deltaTimer = 0;
                }
            }
            if (isMoving && !isBreaking)
            {
                movementTimer -= Time.deltaTime;
                deltaTimer += Time.deltaTime;
                distance = new Vector3(0, 0, speed);
                counter += currentSpeed;

                if (deltaTimer >= 1)
                {
                    movementTimer = 0;
                }
                if (movementTimer <= 0)
                {
                    isMoving = false;
                    movementTimer = 0.5f;
                    deltaTimer = 0;
                    currentSpeed = 0;
                    speed = 2f;
                }
            }



            countText.text = "MovementCounter:" + counter.ToString();
            //TODO: Replace this shit with an actual level editor
            //    if (!firstBuildingCreated)
            //    {
            //        if (counter >= 200)
            //        {
            //            CreateBuilding(new Vector3(0, 2, 0));
            //            firstBuildingCreated = true;
            //        }
            //    }
            //    if (!secondBuildingCreated)
            //    {
            //        if (counter >= 300)
            //        {
            //            CreateBuilding(new Vector3(2, 2, 0));
            //            secondBuildingCreated = true;
            //        }
            //    }
            //    if (!thirdBuildingCreated)
            //    {
            //        if (counter >= 400)
            //        {
            //            CreateBuilding(new Vector3(-2, 2, 0));
            //            thirdBuildingCreated = true;
            //        }
            //    }
            //    if (!fourthBuildingCreated)
            //    {
            //        if (counter >= 500)
            //        {
            //            CreateBuilding(new Vector3(0, 2, 0));
            //            fourthBuildingCreated = true;
            //        }
            //    }
            //    if (!fithBuildingCreated)
            //    {
            //        if (counter >= 600)
            //        {
            //            CreateBuilding(new Vector3(0, 2, 0));
            //            fithBuildingCreated = true;
            //        }
            //    }
            //    if (!sixthBuildingCreated)
            //    {
            //        if (counter >= 700)
            //        {
            //            CreateBuilding(new Vector3(2, 2, 0));
            //            sixthBuildingCreated = true;
            //        }
            //    }
            //    if (!seventhBuildingCreated)
            //    {
            //        if (counter >= 800)
            //        {
            //            CreateBuilding(new Vector3(-2, 2, 0));
            //            seventhBuildingCreated = true;
            //        }
            //    }
            //    if (!eighthBuildingCreated)
            //    {
            //        if (counter >= 900)
            //        {
            //            CreateBuilding(new Vector3(2, 2, 0));
            //            eighthBuildingCreated = true;
            //        }
            //    }
            //    if (!ninethBuildingCreated)
            //    {
            //        if (counter >= 1000)
            //        {
            //            CreateBuilding(new Vector3(0, 2, 0));
            //            ninethBuildingCreated = true;
            //        }
            //    }
            //    if (!tenthBuildingCreated)
            //    {
            //        if (counter >= 1100)
            //        {
            //            CreateBuilding(new Vector3(0, 2, 0));
            //            tenthBuildingCreated = true;
            //        }
            //    }
        }
    }

    //private bool firstBuildingCreated = false;
    //private bool secondBuildingCreated = false;
    //private bool thirdBuildingCreated = false;
    //private bool fourthBuildingCreated = false;
    //private bool fithBuildingCreated = false;
    //private bool sixthBuildingCreated = false;
    //private bool seventhBuildingCreated = false;
    //private bool eighthBuildingCreated = false;
    //private bool ninethBuildingCreated = false;
    //private bool tenthBuildingCreated = false;

    void CreateBuilding(Vector3 buildingPosition )
    {
        

        buildingObject = CommonObjectPoolerScript.current.GetPooledObject();
        if (buildingObject == null)
        {
            return;
        }
        buildingObject.transform.position = buildingPosition;
        buildingObject.transform.localScale = new Vector3(0,0,0);
        buildingObject.SetActive(true);
        
    }

}
