﻿using UnityEngine;
using System.Collections;

public class PopupText : MonoBehaviour
{
    //GameObject scoreIndicator;
    public int score =0; //Should be set to what the current GameObject's score is
    private TextMesh scoreMesh;
    private MeshRenderer textRenderer;
    private float duration = 2.0f;
    private bool isSeen = false;
    private float lerpTime;
    private Vector3 LastPosition;
    private Vector3 newPosition;
    // Use this for initialization
    void Start ()
    {
        //scoreIndicator.AddComponent<TextMesh>();
        scoreMesh= gameObject.GetComponent<TextMesh>();
        scoreMesh.text = score.ToString();
        //scoreIndicator.AddComponent<MeshRenderer>();
        textRenderer.material.color = Color.yellow;
        textRenderer = gameObject.GetComponent<MeshRenderer>();
        LastPosition = transform.position;
        
    }
	
	// Update is called once per frame
	void Update () {
        //if (building/enemy/whatever is destroyed)
        //{
        //       scoreIndicator.GetComponent<MeshRenderer>().enabled = true;
        //       scoreIndicator.transform.position = transform.position;
        //       scoreIndicator.transform.position+=new Vector3(0,10);
        //    isSeen = true;
        //}
        
        if (isSeen)
	    {
	        duration -= Time.deltaTime;
	        lerpTime += Time.deltaTime;

            transform.position = Vector3.Lerp(LastPosition, LastPosition + newPosition, lerpTime);

	        if (duration <= 0)
	        {
	            gameObject.GetComponent<MeshRenderer>().enabled = false;
                isSeen = false;
	            duration = 2.0f;
	        }
	    }

	}

    public void ScorePop(int acqScore, Vector3 acqPosition, Vector3 acqNewPos)
    {
        gameObject.GetComponent<MeshRenderer>().enabled = true;
        transform.position = acqPosition;
        LastPosition = transform.position;
        score = acqScore;
        scoreMesh.text = score.ToString();
        newPosition = acqNewPos;
        //scoreIndicator.transform.position = transform.position;
        //scoreIndicator.transform.position += new Vector3(0, 10);
        isSeen = true;
    }
}
