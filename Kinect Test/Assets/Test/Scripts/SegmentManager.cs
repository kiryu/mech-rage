﻿using UnityEngine;
using System.Collections;

public class SegmentManager : MonoBehaviour {


    private GameObject segmentOne;
    private GameObject segmentTwo;
    private GameObject segmentThree;
    private GameObject segmentOneBuildingOne;
    private GameObject segmentOneBuildingTwo;
    private GameObject segmentOneBuildingThree;
    private GameObject segmentOneBuildingFour;
    private GameObject segmentTwoBuildingOne;
    private GameObject segmentTwoBuildingTwo;
    private GameObject segmentTwoBuildingThree;
    private GameObject segmentTwoBuildingFour;
    private GameObject segmentThreeBuildingOne;
    private GameObject segmentThreeBuildingTwo;
    private GameObject segmentThreeBuildingThree;
    private GameObject segmentThreeBuildingFour;
    // Use this for initialization
    void Start ()
    {
        segmentOne = GameObject.Find("BetaLevelLowPoly");
        segmentTwo = GameObject.Find("BetaLevelLowPoly2");
        segmentThree = GameObject.Find("BetaLevelLowPoly3");

        segmentOneBuildingOne = GameObject.Find("SmashBuildingBlocking");
        //segmentOneBuildingTwo = GameObject.Find("Punchable2");
        //segmentOneBuildingThree = GameObject.Find("Punchable3");
        //segmentOneBuildingFour = GameObject.Find("Punchable4");

        segmentTwoBuildingOne = GameObject.Find("SmashBuildingBlocking2");
        //segmentTwoBuildingTwo = GameObject.Find("Punchable7");
        //segmentTwoBuildingThree = GameObject.Find("Punchable8");
        //segmentTwoBuildingFour = GameObject.Find("Punchable9");

        segmentThreeBuildingOne = GameObject.Find("SmashBuildingBlocking3");
        //segmentThreeBuildingTwo = GameObject.Find("Punchable11");
        //segmentThreeBuildingThree = GameObject.Find("Punchable12");
        //segmentThreeBuildingFour = GameObject.Find("Punchable13");
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (segmentOne.activeSelf == false)
        {
            Debug.Log("segment false");
            segmentOne.SetActive(true);
            segmentOneBuildingOne.SetActive(true);
            //segmentOneBuildingTwo.SetActive(true);
            //segmentOneBuildingThree.SetActive(true);
            //segmentOneBuildingFour.SetActive(true);
        }

        if (segmentTwo.activeSelf == false)
        {
            segmentTwo.SetActive(true);
            segmentTwoBuildingOne.SetActive(true);
            //segmentTwoBuildingTwo.SetActive(true);
            //segmentTwoBuildingThree.SetActive(true);
            //segmentTwoBuildingFour.SetActive(true);
        }

        if (segmentThree.activeSelf == false)
        {
            segmentThree.SetActive(true);
            segmentThreeBuildingOne.SetActive(true);
            //segmentThreeBuildingTwo.SetActive(true);
            //segmentThreeBuildingThree.SetActive(true);
            //segmentThreeBuildingFour.SetActive(true);
        }
    }
}
