﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BloodEffect : MonoBehaviour
{
    private GameObject acqPlayer;
    private PlayerManager player;
    private Color alphaColor;

	// Use this for initialization
	void Start ()
	{
        acqPlayer = GameObject.Find("Player");
	    player = acqPlayer.GetComponent<PlayerManager>();
        alphaColor = new Color(0,0,0,0.2f);

	}
	
	// Update is called once per frame
	void Update ()
    {

	    if (player.takingDamage && player.Health >= 80)
	    {
            alphaColor = new Color(0, 0, 0, 0.2f);
            gameObject.GetComponent<Image>().color = alphaColor;
	    }

        if (player.takingDamage && player.Health >= 60 && player.Health <= 80)
        {
            alphaColor = new Color(0, 0, 0, 0.4f);
            gameObject.GetComponent<Image>().color = alphaColor;
        }

        if (player.takingDamage && player.Health >= 40 && player.Health <= 60)
        {
            alphaColor = new Color(0, 0, 0, 0.6f);
            gameObject.GetComponent<Image>().color = alphaColor;
        }

        if (player.takingDamage && player.Health >= 20 && player.Health <= 40)
        {
            alphaColor = new Color(0, 0, 0, 0.8f);
            gameObject.GetComponent<Image>().color = alphaColor;
        }

        if (!player.takingDamage)
	    {
            alphaColor = new Color(0, 0, 0, 0);
            gameObject.GetComponent<Image>().color = alphaColor;
        }
	
	}
}
