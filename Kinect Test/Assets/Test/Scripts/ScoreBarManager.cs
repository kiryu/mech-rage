﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreBarManager : MonoBehaviour
{

    public static int score;
    Text scoreText;
    // Use this for initialization

    void Awake()
    {
        scoreText = GetComponent<Text>();
        score = 0;
    }

    // Update is called once per frame
    void Update()
    {
        scoreText.text = "" + score;
    }
}
