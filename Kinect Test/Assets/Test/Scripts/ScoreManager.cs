﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class ScoreManager : MonoBehaviour {
    Dictionary<string, Dictionary<string, int>> playerScores;

    void Start()
    {


    }

    void Init()
    {
        if (playerScores != null)
        {
            return;
        }
        playerScores = new Dictionary<string, Dictionary<string, int>>();
    }
    public int GetScore(string username, string scoreType)
    {
        Init();
        if (playerScores.ContainsKey(username) == false)
        {
            return 0;
        }
        if (playerScores.ContainsKey(scoreType) == false)
        {
            return 0;
        }
        return playerScores[username][scoreType];
    }
    public void SetScore(string username, string scoreType, int value) //Sets a score for a new name
    {
        Init();
        if (playerScores.ContainsKey(username) == false) //If the player isn't already on the list, create a dictionary entry for it
        {
            playerScores[username] = new Dictionary<string, int>();
        }
        playerScores[username][scoreType] = value;
    }
    public void ChangeScore(string username, string scoreType, int value) //Changes the score of an existing entry
    {
        Init();
        int currentScore = GetScore(username, scoreType);
        SetScore(username, scoreType, currentScore + value);
    }
    public string[] GetPlayerNames() //Returns player names as an array of strings
    {
        Init();
        return playerScores.Keys.ToArray();
    }

}
