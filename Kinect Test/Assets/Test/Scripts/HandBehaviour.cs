﻿using UnityEngine;
using System.Collections;

public class HandBehaviour : MonoBehaviour {

    private BodySourceView bodySource;
    private GameObject kinectBody;
    private Vector3 newRotation;
    private Vector3 newPosition;
    private Vector3 offset;
    private Vector3 rotateOffset;
    private Vector3 xReset;
    private float numberOne;

    public enum BODYTYPE
    {
        RIGHTHAND,
        RIGHTFOREARM,
        RIGHTBACKARM,
        LEFTHAND,
        LEFTFOREARM,
        LEFTBACKARM
    }

    public BODYTYPE myType;

    // Use this for initialization
    void Start ()
    {
        kinectBody = GameObject.Find("BodyView");
        bodySource = kinectBody.GetComponent<BodySourceView>();
        newPosition = new Vector3(40,-5,61);
        offset = new Vector3(-2, 0);
        rotateOffset = new Vector3(150,150,150);
        xReset = new Vector3(-1,1);
        numberOne = 1;
    }
	
	// Update is called once per frame
	void Update ()
	{
	    switch (myType)
	    {
                case BODYTYPE.RIGHTHAND:
	            bodySource.rightHandWristPos.x *= -1;
	            transform.position = bodySource.rightHandWristPos + newPosition + offset;
	            transform.right *= -1f;
	            //transform.localScale = (bodySource.rightHandWristPos / 20);

                newRotation = Vector3.RotateTowards(bodySource.rightWristRota, bodySource.rightHandRota * 2 + rotateOffset, 2, 2);
                transform.rotation = Quaternion.LookRotation(newRotation * 2);
                break;
                case BODYTYPE.RIGHTFOREARM:
	            bodySource.rightWristElbowPos.x *= -1;
	            transform.position = bodySource.rightWristElbowPos + newPosition + offset;
                transform.right *= -1f;
                newRotation = Vector3.RotateTowards(bodySource.rightElbowRota, bodySource.rightWristRota *2 + rotateOffset, 2, 2);
                transform.rotation = Quaternion.LookRotation(newRotation * 2);
                //transform.rotation.SetFromToRotation(bodySource.rightWristRota, bodySource.rightElbowRota);
                break;
                case BODYTYPE.RIGHTBACKARM:
	            bodySource.rightElbowShoulderPos.x *= -1;
	            transform.position = bodySource.rightElbowShoulderPos + newPosition + offset;
                transform.right *= -1f;
                newRotation = Vector3.RotateTowards(bodySource.rightShoulderRota, bodySource.rightElbowRota*2 + rotateOffset, 2, 2);
                transform.rotation = Quaternion.LookRotation(newRotation*2);
                //transform.rotation.SetFromToRotation(bodySource.rightElbowRota, bodySource.rightShoulderRota);
                break;
                case BODYTYPE.LEFTHAND:
	            bodySource.leftHandWristPos.x *= -1;
	            transform.position = bodySource.leftHandWristPos + newPosition - offset;
                newRotation = Vector3.RotateTowards(-bodySource.leftWristRota, -bodySource.leftHandRota*2 - rotateOffset, 2, 2);
                transform.rotation = Quaternion.LookRotation(newRotation*2);
	            
                //transform.rotation.SetFromToRotation(bodySource.leftHandRota, bodySource.leftWristRota);
                break;
                case BODYTYPE.LEFTFOREARM:
	            bodySource.leftWristElbowPos.x *= -1;
	            transform.position = bodySource.leftWristElbowPos + newPosition - offset;
                newRotation = Vector3.RotateTowards(-bodySource.leftElbowRota, -bodySource.leftWristRota*2 - rotateOffset, 2, 2);
                transform.rotation = Quaternion.LookRotation(newRotation*2);
                //transform.rotation.SetFromToRotation(bodySource.leftWristRota, bodySource.leftElbowRota);
                break;
                case BODYTYPE.LEFTBACKARM:
	            bodySource.leftElbowShoulderPos.x *= -1;
	            transform.position = bodySource.leftElbowShoulderPos + newPosition - offset;
                newRotation = Vector3.RotateTowards(-bodySource.leftShoulderRota, -bodySource.leftElbowRota*2 - rotateOffset, 2, 2);
                transform.rotation = Quaternion.LookRotation(newRotation*2);
                //transform.rotation.SetFromToRotation(bodySource.leftElbowRota, bodySource.leftShoulderRota);
                break;
	    }
	}
}
