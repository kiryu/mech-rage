﻿using UnityEngine;
using System.Collections;

public class EnemyBehaviour : MonoBehaviour {


    private Vector3 lastPosition;
    private GameObject acqBuildingManager;
    private BuildingManager buildingManager;
    private GameObject acqPlayer;
    private PlayerManager playerManager;
    private GameObject acqMuzzleFlash;
    private MuzzleFlash muzzleFlash;
    private GameObject acqEnemyManager;
    private EnemyManager enemyManager;
    private float deltaTimer;
    private Vector3 muzzlePos;
    private float speed;
    private Vector3 direction;
    private bool movementDirection;
    private GameObject acqMuzzleFlashManager;
    private CommonObjectPoolerScript muzzleFlashPool;
    private GameObject acqRageBar;
    private RageBehaviour rageBehaviour;
    GameObject muzzleObject;
    private GameObject heliParticle;
    private GameObject tankParticle;
    private bool shootDelay;
    private GameObject acqScorePopup;
    private PopupText popupText;
    
    

    public bool isShooting;

    public enum FACING
    {
        LEFT,
        RIGHT
    }

    public enum ENEMYTYPE
    {
        HELI,
        TANK
    }

    public FACING myFacing;

    public ENEMYTYPE myEnemytype;

    void OnTriggerEnter(Collider coll)
    {
        if (coll.gameObject.tag == "AttackBox")
        {
          
            
            Debug.Log("hit");
            enemyManager.enemiesLeft -= 1;
            
            muzzleFlash.beingFired = false;
            muzzleFlash.shootingLoop = false;
            coll.gameObject.GetComponent<MultipleSounds>().audioClip2.pitch = Random.Range(0.8f, 1.2f);
            coll.gameObject.GetComponent<MultipleSounds>().audioClip2.Play();
            muzzleObject.SetActive(false);
            rageBehaviour.rage += 20;
            switch (myEnemytype)
            {
                    case ENEMYTYPE.HELI:
                    heliParticle.transform.position = transform.position;
                    heliParticle.SetActive(true);
                    heliParticle.GetComponent<ParticleSystem>().Play();
                    Debug.Log(heliParticle.transform.position);
                    ScoreBarManager.score += 100;
                    gameObject.SetActive(false);
                    popupText.ScorePop(100, transform.position, new Vector3(0, 5));
                    break;

                    case ENEMYTYPE.TANK:
                    tankParticle.transform.position = transform.position;
                    tankParticle.GetComponent<ParticleSystem>().Play();
                    ScoreBarManager.score += 50;
                    gameObject.SetActive(false);
                    popupText.ScorePop(50, transform.position, new Vector3(0, 10));
                    break;
            }
            
        }
    }

    // Use this for initialization
    void Start ()
    {
        acqBuildingManager = GameObject.Find("BuildingManager");
        acqMuzzleFlash = GameObject.Find("Muzzle Flash");
        acqMuzzleFlashManager = GameObject.Find("Muzzle Flash Manager");
        acqRageBar = GameObject.Find("RageBehaviour");
        rageBehaviour = acqRageBar.GetComponent<RageBehaviour>();
        //transform.position = new Vector3(0,0,0);
        lastPosition = transform.position;
        buildingManager = acqBuildingManager.GetComponent<BuildingManager>();
        acqPlayer = GameObject.Find("Player");
        playerManager = acqPlayer.GetComponent<PlayerManager>();
        deltaTimer = 1;
        isShooting = false;
        acqEnemyManager = GameObject.Find("EnemyManager");
        enemyManager = acqEnemyManager.GetComponent<EnemyManager>();
        muzzlePos = new Vector3(0,-4,2);
        muzzleFlash = acqMuzzleFlash.GetComponent<MuzzleFlash>();
        muzzleFlashPool = acqMuzzleFlashManager.GetComponent<CommonObjectPoolerScript>();
        speed = 1;
        direction = new Vector3(speed, 0,0);
        movementDirection = false;
        heliParticle = GameObject.Find("HelicopterDebries");
        tankParticle = GameObject.Find("TankDestroy");
        acqScorePopup = GameObject.Find("ScorePopup");
        popupText = acqScorePopup.GetComponent<PopupText>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        lastPosition = transform.position;
        if (buildingManager.isMoving)
        {
            transform.position = Vector3.Lerp(lastPosition, lastPosition + buildingManager.distance,
                Mathf.Min(buildingManager.deltaTimer * (buildingManager.currentSpeed * 2), 1));
            //Debug.Log(buildingManager.currentSpeed);

        }
        if (!buildingManager.isMoving)
        {
            lastPosition = transform.position;
        }

	    switch (myEnemytype)
	    {
	            case ENEMYTYPE.HELI:
                muzzlePos = new Vector3(0, -4, 2);
                break;
                case ENEMYTYPE.TANK:
                muzzlePos = new Vector3(0, 0, 2);
                break;
	    }


	    
	    if (myEnemytype == ENEMYTYPE.HELI)
	    {
            transform.position += direction;

            if (transform.position.x >= 20)
	        {
	            speed = -Random.value;
                
	            direction = new Vector3(speed, 0, 0);

	        }
	        if (transform.position.x <= -20)
	        {
	            speed = Random.value;
	            direction = new Vector3(speed, 0, 0);

	        }
	    }
	    if (myEnemytype == ENEMYTYPE.HELI)
	    {


	        if (transform.position.x >= 1)
	        {
	            gameObject.GetComponent<Animator>().Play("HeliAnimRight");
	            myFacing = FACING.RIGHT;
	        }
	        else
	        {
	            gameObject.GetComponent<Animator>().Play("HeliAnimLeft");
	            myFacing = FACING.LEFT;
	        }
	    }

	    if (myEnemytype == ENEMYTYPE.HELI)
	    {


	        if (transform.position.z >= 500)
	        {
	            transform.position = new Vector3(1.16f, 5.57f, -200);
	        }
	    }

	    if (!isShooting)
	    {

	        switch (myEnemytype)
	        {

                case ENEMYTYPE.HELI:
	                if (transform.position.z + 90 >= acqPlayer.transform.position.z)
	                {
	                    playerManager.Health -= 1;
	                    playerManager.takingDamage = true;
	                    rageBehaviour.rage += 2;
	                    isShooting = true;
	                    CreateMuzzleFlash();
	                    muzzleObject.transform.position = transform.position + muzzlePos;
	                    muzzleFlash.beingFired = true;

	                }
                    break;

                    case ENEMYTYPE.TANK:
                    if (transform.position.z + 70 >= acqPlayer.transform.position.z)
                    {
                        playerManager.Health -= 1;
                        playerManager.takingDamage = true;
                        rageBehaviour.rage += 2;
                        isShooting = true;
                        CreateMuzzleFlash();
                        muzzleObject.transform.position = transform.position + muzzlePos;
                        muzzleFlash.beingFired = true;

                    }
                    break;
	        }
	    }
	    if (!shootDelay)
	    {


	        if (isShooting)
	        {
	            deltaTimer -= Time.deltaTime;

	            muzzleObject.transform.position = transform.position + muzzlePos;



	            if (deltaTimer <= 0)
	            {
	                deltaTimer = 1;
	                shootDelay = true;
	                muzzleFlash.shootingLoop = false;
	                muzzleObject.SetActive(false);
	                muzzleFlash.beingFired = false;
	            }

	        }
	    }

	    if (shootDelay)
	    {
            deltaTimer -= Time.deltaTime;
	        if (deltaTimer <= 0)
	        {
                deltaTimer = 1;
	            isShooting = false;
	            shootDelay = false;
	        }
        }
    }

    void CreateMuzzleFlash()
    {
        
        muzzleObject = muzzleFlashPool.GetPooledObject();
        if (muzzleObject == null)
        {
            return;
        }
        muzzleObject.SetActive(true);

    }
}
