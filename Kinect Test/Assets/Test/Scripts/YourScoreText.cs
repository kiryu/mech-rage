﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class YourScoreText : MonoBehaviour {
    int playScore = 0;
    Text scoreText;
	// Use this for initialization
	void Start () {
        scoreText = GetComponent<Text>();
        playScore = PlayerPrefs.GetInt("Player Score");
        scoreText.text = playScore.ToString();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
