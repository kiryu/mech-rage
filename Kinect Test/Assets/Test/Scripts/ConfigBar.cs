﻿using UnityEngine;
using System.Collections;

public class ConfigBar : MonoBehaviour
{

    private float configTime;
    private MenuState menuState;
    private Vector3 startPosition;
    private Vector3 startScale;

	// Use this for initialization
    void Start()
    {
        menuState = gameObject.GetComponentInParent<MenuState>();
        configTime = 0;
        startPosition = transform.localPosition;
        startScale = transform.localScale;
    }

    // Update is called once per frame
	void Update ()
    {
	    if (!menuState.kinectConfig && menuState.bodyIsTracked)
	    {
	        
	            configTime += Time.deltaTime;
	            transform.localScale += new Vector3(configTime*0.001f, 0);
	            transform.localPosition += new Vector3(configTime*0.0013f, 0);
	        
	    }

	    if (!menuState.kinectConfig && !menuState.bodyIsTracked)
	    {
	        transform.localPosition = startPosition;
	        transform.localScale = startScale;
	    }

	    
    }
}
