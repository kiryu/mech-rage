﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    private GameObject Player;
    private PlayerManager playerManager;
    private int currentHealth;
    private Vector3 newPosition;
    private GameObject acqStateManager;
    private StateManager stateManager;

    public Text loseText;
	// Use this for initialization
	void Start ()
	{
	    Player = GameObject.Find("Player");
	    playerManager = Player.GetComponent<PlayerManager>();
	    currentHealth = playerManager.Health;
	    newPosition = transform.localPosition;
	    loseText.text = "";
        acqStateManager = GameObject.Find("StateManager");
        stateManager = acqStateManager.GetComponent<StateManager>();
    }
	
	// Update is called once per frame
	void Update () {
	    if (currentHealth > playerManager.Health)
	    {
	        newPosition.x -= currentHealth - playerManager.Health;

            transform.localPosition = new Vector3(newPosition.x, transform.localPosition.y);
	        currentHealth = playerManager.Health;
	    }

        if (currentHealth < playerManager.Health)
        {
            transform.position = new Vector3(currentHealth + playerManager.Health, transform.position.y);
            currentHealth = playerManager.Health;
        }
	    if (playerManager.Health == 0)
	    {
            //grab building manager and enemymanager and turn them off
	        loseText.text = "YOU LOSE";
            //stateManager.CurrentState = StateManager.STATE.MENUSTATE;

            //EditorSceneManager.OpenScene("Score Screen");
            SceneManager.LoadScene("Score Screen");
            //create an end scene when the player dies or wins and then have some sort of input that returns them to the main scene
        }


    }
}
