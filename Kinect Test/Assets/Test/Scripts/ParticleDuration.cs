﻿using UnityEngine;
using System.Collections;

public class ParticleDuration : MonoBehaviour
{


    public float maxDuration;
    private float savedDuration;

	// Use this for initialization
	void Start ()
	{

	    savedDuration = maxDuration;
	}
	
	// Update is called once per frame
	void Update ()
	{

	    if (gameObject.GetComponent<ParticleSystem>().isPlaying)
	    {
	        maxDuration -= Time.deltaTime;
	        if (maxDuration <= 0)
	        {

	            maxDuration = savedDuration;
	            gameObject.GetComponent<ParticleSystem>().Stop();
	            gameObject.SetActive(false);
	        }

	    }
	    
	}
}
