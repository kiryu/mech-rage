﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(SpriteRenderer))]

public class Tiling : MonoBehaviour {

    public int offsetX = 2;

    public bool hasARightBody = false;
    public bool hasALeftBody= false;

    public bool reverseScale=false;         //used is object is not tileble

    private float spriteWidth= 0f;          // the width of our element
    private Camera cam;
    private Transform myTransform;

    void Awake()
    {
        cam = Camera.main;
        myTransform = transform;
    }

    



	// Use this for initialization
	void Start () {
        SpriteRenderer sRenderer = GetComponent<SpriteRenderer>();
        spriteWidth = sRenderer.sprite.bounds.size.x;
	}
	
	// Update is called once per frame
	void Update () {
        //does it still need bodys if not do nothing
        if (hasALeftBody || hasARightBody == false)
        {
            //calculate the cameras extend(half the width) of what the camera can see in the world coordinates 
            float camHorizontalExtend = cam.orthographicSize * Screen.width / Screen.height;
            //calculate the x position where the camerea can see the edge of the sprite (element)
            float edgeVisiblePositionRight = (myTransform.position.x + spriteWidth / 2) - camHorizontalExtend;
            float edgeVisiblePositionLeft = (myTransform.position.x - spriteWidth / 2) + camHorizontalExtend;

            //checking if we can see the edge of the element and then calling makeNewBody if we can
            if(cam.transform.position.x >=edgeVisiblePositionRight - offsetX && hasARightBody == false)
            {
                MakeNewBuddy(1);
                hasARightBody = true;
            }
            else if(cam.transform.position.x <=edgeVisiblePositionLeft + offsetX && hasALeftBody==false)
            {
                MakeNewBuddy(-1);
                hasARightBody = true; 
            }
           
        }
	}

    //a function that creates a buddy on the side requierd
    void MakeNewBuddy(int rightOrLeft)
    {
        //calculateing the position of our body
        Vector3 newPosition = new Vector3(myTransform.transform.position.x + spriteWidth * rightOrLeft,myTransform.position.y,myTransform.position.z);
        //instatiating or new body and storing him in a new variable
        Transform newBuddy = Instantiate(myTransform,newPosition,myTransform.rotation) as Transform;


        //if not tilible lets reverse the x size to get rid of ugly seams
        if (reverseScale == true)
        {
            newBuddy.localScale = new Vector3(newBuddy.localScale.x * -1, newBuddy.localScale.y, newBuddy.localScale.z);
        }
        newBuddy.parent = myTransform.parent;

        if(rightOrLeft >= 0 )
        {
            newBuddy.GetComponent<Tiling>().hasALeftBody = true;
        }
        else
        {
            newBuddy.GetComponent<Tiling>().hasARightBody = true;
        }
    } 

}
