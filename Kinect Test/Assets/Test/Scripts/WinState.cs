﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
//using UnityEngine.SceneManager;

public class WinState : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

    }

    void Win()
    {
        ScoreBarManager.score += 20000; //Points for winning
        PlayerPrefs.SetInt("Player Score ", ScoreBarManager.score); //ScoreBarManager is a recent upload to bitbucket, score is public static
        //SceneManager.LoadScene("HighScore");
    }
}
