﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CommonObjectPoolerScript : MonoBehaviour
{

    public static CommonObjectPoolerScript current;
    public GameObject pooledObject;
    public int pooledAmount = 20;
    public bool willGrow = true;

    private List<GameObject> pooledObjects;

    void Awake()
    {
        current = this;
    }

	// List of undefined objects added here, set to false so it isn't immediately put into the game
	void Start ()
	{
	    pooledObjects = new List<GameObject>();
	    for (int i = 0; i < pooledAmount; i++) //fills the list with GameObjects
	    {
	        GameObject obj = (GameObject) Instantiate(pooledObject);
            obj.SetActive(false);
	        pooledObjects.Add(obj);
	    }
	}

    public GameObject GetPooledObject()  //Accesses an inactive object from the list
    {
        for (int i = 0; i < pooledObjects.Count; i++)
        {
            if (!pooledObjects[i].activeInHierarchy)
            {
                return pooledObjects[i];
            }

        }
        if (willGrow) //If we are allowed to add more objects to a pool mid-game, instantiate one here
        {
            GameObject obj = (GameObject) Instantiate(pooledObject);
            pooledObjects.Add(obj);
            return obj;
        }
        return null;
    }
	
	// Update is called once per frame

}
