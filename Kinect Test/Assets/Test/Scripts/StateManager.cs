﻿using UnityEngine;
using System.Collections;

public class StateManager : MonoBehaviour {

    public enum STATE
    {
        MENUSTATE,
        GAMESTATE,
        HIGHSCORESTATE
    }

    public STATE CurrentState;

    // Use this for initialization
	void Start ()
    {
	CurrentState = STATE.MENUSTATE;
	}
	
	// Update is called once per frame
	void Update ()
    {
	
	}
}
