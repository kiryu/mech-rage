﻿using UnityEngine;
using System.Collections;

public class MuzzleFlash : MonoBehaviour {

    private GameObject acqBuildingManager;
    private BuildingManager buildingManager;
    private Vector3 lastPosition;
    private Animator anim;
    private float soundPitchRand = 0.10f;
    public bool beingFired;
    public bool heliShooting;
    public bool shootingLoop;

    // Use this for initialization
    void Start ()
    {
        acqBuildingManager = GameObject.Find("BuildingManager");
        anim = GetComponent<Animator>();
        lastPosition = transform.position;
        buildingManager = acqBuildingManager.GetComponent<BuildingManager>();
        beingFired = false;
        
    }
	
	// Update is called once per frame
	void Update ()
    {
        lastPosition = transform.position;
        if (buildingManager.isMoving)
        {
            transform.position = Vector3.Lerp(lastPosition, lastPosition + buildingManager.distance,
                Mathf.Min(buildingManager.deltaTimer * buildingManager.currentSpeed, 1));
            //Debug.Log(buildingManager.currentSpeed);

        }
        if (!buildingManager.isMoving)
        {
            lastPosition = transform.position;
        }


	    if (beingFired)
	    {
	        if (!shootingLoop)
	        {
	            shootingLoop = true;
                gameObject.GetComponent<AudioSource>().pitch = Random.Range(1f - soundPitchRand, 1f + soundPitchRand); //Randomizes pitch
                gameObject.GetComponent<AudioSource>().Play();
            }
            //gameObject.GetComponent<AudioSource>().Play();
	        //anim.Play("muzzle sheet", 0);
	    }

	    if (!beingFired && !shootingLoop)
	    {
	        gameObject.GetComponent<AudioSource>().Stop();
	    }

	    //if (!beingFired)
	    //{
	    //    gameObject.GetComponent<AudioSource>().Stop();
     //       anim.Stop();
	    //}
    }
}
