﻿using UnityEngine;
using System.Collections;

public class HighScoreCheckScript : MonoBehaviour {
    int currentHighScore;
    int playerScore;
	// Use this for initialization
	void Start () {
        if (PlayerPrefs.GetInt("HighScore")!=null)
        {
            currentHighScore = PlayerPrefs.GetInt("HighScore");
        }
        else
        {
            PlayerPrefs.SetInt("HighScore", 0);
        }
        playerScore = PlayerPrefs.GetInt("Player Score");

        ScoreCheck();
 
        
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void ScoreCheck ()
    {
        if (playerScore>currentHighScore)
        {
            currentHighScore = playerScore;
            PlayerPrefs.SetInt("HighScore", currentHighScore);

        }
    }
}
