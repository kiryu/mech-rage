﻿using UnityEngine;
using System.Collections;

public class RageBehaviour : MonoBehaviour {


    //when the player gets hit, gain a little rage. when the player destroyes something, gain rage.
    public float rage;
    private float currentRage;
    private Vector3 newPosition;

	// Use this for initialization
	void Start ()
	{
	    rage = 0;
	    newPosition = transform.localPosition;
	    currentRage = rage;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (rage >= 100)
        {
            rage = 100;
            
        }

        if (currentRage > rage)
	    {
	        newPosition.y -= (currentRage - rage) / 2;
            transform.localPosition = new Vector3(transform.localPosition.x, newPosition.y);
	        newPosition = transform.localPosition;
	        currentRage = rage;
	    }
	    if (currentRage < rage)
	    {
	        newPosition.y -= (currentRage - rage) / 2;
            transform.localPosition = new Vector3(transform.localPosition.x, newPosition.y);
            newPosition = transform.localPosition;
            currentRage = rage;
        }

	    
	
	}
}
