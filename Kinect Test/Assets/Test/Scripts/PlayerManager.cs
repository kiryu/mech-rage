﻿using UnityEngine;
using System.Collections;

public class PlayerManager : MonoBehaviour
{
    public int Health = 100;
    public bool takingDamage = false;
    private float damageTimer;
	// Use this for initialization
	void Start ()
	{
	    damageTimer = 1.0f;
	}
	
	// Update is called once per frame
	void Update () {
	    //if (Input.GetKeyDown(KeyCode.H))
	    //{
	    //    Health -= 1;
	    //}
        //Debug.Log(Health);

	    if (takingDamage)
	    {
	        damageTimer -= Time.deltaTime;
	        if (damageTimer <= 0)
	        {
	            damageTimer = 1.0f;
	            takingDamage = false;
	        }
	    }
	}
}
