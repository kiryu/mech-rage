﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HighScoreText : MonoBehaviour {
    int playScore = 0;
    Text scoreText;
    // Use this for initialization
    void Start () {
	        scoreText = GetComponent<Text>();
        playScore = PlayerPrefs.GetInt("HighScore");
        scoreText.text = playScore.ToString();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
