﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
public class LoseStateCheck : MonoBehaviour {
    PlayerManager playerManager;
    Vector3 deathPosition;
    float deathTimer = 5;
    
	// Use this for initialization
	void Start () {
        deathPosition = new Vector3(Camera.main.transform.position.x, Camera.main.transform.position.y, Camera.main.transform.position.z);
        deathPosition.y -= 5;
        playerManager = GameObject.FindObjectOfType<PlayerManager>();
	}
	
	// Update is called once per frame
	void Update () {
	if (playerManager.Health<=0)
        {
            Camera.main.transform.position = Vector3.Lerp(Camera.main.transform.position,deathPosition , 1.0f);
            deathTimer -= Time.deltaTime;
        }

        if (Camera.main.transform.position==deathPosition)
        {
            //Show death text and image before the scene changes
        }
        if (deathTimer<=0)
        {
            PlayerPrefs.SetInt("Player Score ", ScoreBarManager.score); //ScoreBarManager is a recent upload to bitbucket, score is public static
            SceneManager.LoadScene("High Score Screen"); //Change to whatever you call the score screen
        }

	}
}
